#include <stdio.h>
int main() 
{
    int x, y, t;
    printf("Enter x: ");
    scanf("%i", &x);
    printf("Enter y: ");
    scanf("%i", &y);
    t = x;
    x = y;
    y = t;
    printf("After swapping,\n x = %i\n y = %i\n", x,y);
    return 0;
}
